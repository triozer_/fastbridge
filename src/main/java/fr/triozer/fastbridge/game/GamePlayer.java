package fr.triozer.fastbridge.game;

import org.bukkit.entity.Player;

import java.util.*;

/**
 * @author Triozer
 */
public class GamePlayer {

    protected static Map<UUID, GamePlayer> players = new HashMap<>();

    private Player         player;
    private GameTeam       team;
    private GameKit        kit;
    private GameScoreboard gameScoreboard;
    private double         point = 0.0;

    public GamePlayer(Player player, GameTeam team, GameKit kit) {
        this.player = player;
        this.team = team;
        this.kit = kit;
        point = 0;

        gameScoreboard = new GameScoreboard(player, "§aFastBridge");
        setupScoreboard();

        players.put(player.getUniqueId(), this);
        team.add(player);
    }

    public void kill() {
        point += 0.25;

        if (point < 2)
            gameScoreboard.setLine(5, "§7Point: §6" + point);
        else
            gameScoreboard.setLine(5, "§7Points: §6" + point);
    }

    public void death() {
        if (point - 0.25 <= 0) {
            point = 0;
        } else {
            point -= 0.25;
        }

        gameScoreboard.setLine(3, "§7Kit: §6" + kit.getName());
        if (point < 2)
            gameScoreboard.setLine(5, "§7Point: §6" + point);
        else
            gameScoreboard.setLine(5, "§7Points: §6" + point);
    }

    private void setupScoreboard() {
        gameScoreboard.create();

        gameScoreboard.setLine(0, "§a");
        gameScoreboard.setLine(1, "§7Chrono: ");
        gameScoreboard.setLine(2, "§b");
        gameScoreboard.setLine(3, "§7Kit: §6" + kit.getName());
        gameScoreboard.setLine(4, "§c");
        if (point < 2)
            gameScoreboard.setLine(5, "§7Point: §60");
        else
            gameScoreboard.setLine(5, "§7Points: §60");
        if (team.getPoint() < 2)
            gameScoreboard.setLine(6, "§7Point (équipe): §60");
        else
            gameScoreboard.setLine(6, "§7Points (équipe): §60");
        gameScoreboard.setLine(7, "§d");
    }

    public void setKit(GameKit kit) {
        this.kit = kit;
    }

    public Player getPlayer() {
        return player;
    }

    public GameTeam getTeam() {
        return team;
    }

    public GameKit getKit() {
        return kit;
    }

    public double getPoint() {
        return point;
    }

    public GameScoreboard getGameScoreboard() {
        return gameScoreboard;
    }

    public static GamePlayer getPlayer(UUID uuid) {
        return players.get(uuid);
    }

    public static List<GamePlayer> getPlayers() {
        List list = new ArrayList<>();

        players.entrySet().forEach(entrySet -> list.add(entrySet.getValue()));

        return list;
    }
}
