package fr.triozer.fastbridge.game;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scoreboard.Team;

import java.util.*;

/**
 * @author Triozer
 */
public class GameTeam {

    public static final Map<String, GameTeam> teams = new HashMap<>(); //NOSONAR
    public static final Map<UUID, GameTeam>   temps = new HashMap<>(); //NOSONAR

    private Team team;

    private List<GamePlayer> players;

    private Location spawnLocation;
    private Location food;
    private Location block;
    private Location hole;

    private int point = 0;

    public GameTeam(String name, String prefix, Location spawnLocation, Location food, Location block, Location hole) {
        team = Game.getGameScoreboard().registerNewTeam(name);

        team.setPrefix(prefix);
        team.setDisplayName(prefix);

        team.setAllowFriendlyFire(false);
        team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.FOR_OTHER_TEAMS);

        this.spawnLocation = spawnLocation;
        this.food = food;
        this.block = block;
        this.hole = hole;

        players = new ArrayList<>();

        teams.put(name, this);
    }

    public void add(Player gamePlayer) {
        players.add(GamePlayer.getPlayer(gamePlayer.getUniqueId()));
        team.addEntry(gamePlayer.getName());
    }

    public void remove(Player gamePlayer) {
        players.remove(GamePlayer.getPlayer(gamePlayer.getUniqueId()));
        team.removeEntry(gamePlayer.getName());
    }

    public void addPoint() {
        point += 1;

        for (GamePlayer gamePlayer : players) {
            if (point < 2)
                gamePlayer.getGameScoreboard().setLine(6, "§7Point (équipe): §6" + point);
            else
                gamePlayer.getGameScoreboard().setLine(6, "§7Points (équipe): §6" + point);
        }
    }

    public void equip() {
        ItemStack[] items = new ItemStack[4];

        items[0] = new ItemStack(Material.LEATHER_HELMET);
        items[1] = new ItemStack(Material.LEATHER_CHESTPLATE);
        items[2] = new ItemStack(Material.LEATHER_LEGGINGS);
        items[3] = new ItemStack(Material.LEATHER_BOOTS);

        for (ItemStack armor : Arrays.asList(items)) {
            LeatherArmorMeta meta = (LeatherArmorMeta) armor.getItemMeta();

            if ("red".equals(getName()))
                meta.setColor(Color.fromRGB(0, 0, 255));
            else if ("blue".equals(getName()))
                meta.setColor(Color.fromRGB(255, 0, 0));

            armor.setItemMeta(meta);
        }

        players.forEach(gamePlayer -> {
            gamePlayer.getPlayer().getInventory().setHelmet(items[0]);
            gamePlayer.getPlayer().getInventory().setChestplate(items[1]);
            gamePlayer.getPlayer().getInventory().setLeggings(items[2]);
            gamePlayer.getPlayer().getInventory().setBoots(items[3]);
        });
    }

    public Team getTeam() {
        return team;
    }

    public List<GamePlayer> getPlayers() {
        return players;
    }

    public String getName() {
        return team.getName();
    }

    public String getPrefix() {
        return team.getPrefix();
    }

    public Location getSpawnLocation() {
        return spawnLocation;
    }

    public Location getFood() {
        return food;
    }

    public Location getHole() {
        return hole;
    }

    public Location getBlock() {
        return block;
    }

    public int getPoint() {
        return point;
    }
}
