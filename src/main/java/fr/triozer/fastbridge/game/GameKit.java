package fr.triozer.fastbridge.game;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @author Triozer
 */
public enum GameKit {
    BASIC("Basic", "Le kit de base !", 0, Material.STONE_SWORD, new ItemStack(Material.STONE_SWORD), new ItemStack(Material.SANDSTONE, 16)),
    DRESSOR("Dresseur", "Attrapez les tous.", 1, Material.MONSTER_EGG, new ItemStack(Material.STONE_SWORD), new ItemStack(Material.SANDSTONE, 16), new ItemStack(Material.EGG)),
    STEALER("Voleur", "Oh, un diamant ! :3", 2, Material.PRISMARINE_SHARD, new ItemStack(Material.STONE_SWORD), new ItemStack(Material.SANDSTONE, 16), new ItemStack(Material.STICK));

    private String      name;
    private String      description;
    private int         slot;
    private Material    icon;
    private ItemStack[] items;

    GameKit(String name, String description, int slot, Material icon, ItemStack... items) {
        this.name = name;
        this.description = description;
        this.slot = slot;
        this.icon = icon;
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getSlot() {
        return slot;
    }

    public Material getIcon() {
        return icon;
    }

    public ItemStack[] getItems() {
        return items;
    }

    public static GameKit random() {
        return values()[new Random().nextInt(values().length)];
    }

    public static Inventory inventory() {
        Inventory inventory = Bukkit.createInventory(null, 36, "§6Kits");

        for (GameKit gameKit : Arrays.asList(values())) {
            ItemStack itemStack = new ItemStack(gameKit.getIcon());
            ItemMeta  itemMeta  = itemStack.getItemMeta();

            itemMeta.setDisplayName("§6" + gameKit.getName());

            List<String> lore = new ArrayList<>();

            lore.add("§r");
            lore.add("§r§6" + gameKit.getDescription());
            lore.add("§r");

            for (int i = 0; i < gameKit.getItems().length; i++) {
                ItemStack current = gameKit.getItems()[i];

                String s = "";

                if (current.getAmount() > 1)
                    s = "S";

                lore.add("§e- §7" + current.getAmount() + " " + current.getType().name().replaceAll("_", " ") + s);
            }

            itemMeta.setLore(lore);

            itemStack.setItemMeta(itemMeta);
            inventory.setItem(gameKit.getSlot(), itemStack);
        }

        return inventory;
    }

    public static void drop(GamePlayer playerKiller) {
        int         itemCount  = playerKiller.getKit().getItems().length;
        ItemStack[] kitItems   = playerKiller.getKit().getItems();
        int         percentage = new Random().nextInt(100) / itemCount * 100;

        List<ItemStack> addItems = new ArrayList<>();

        for (int i = 0; i < itemCount; i++) {
            if (percentage <= 25) {
                if (i == 0)
                    addItems.add(kitItems[new Random().nextInt(itemCount)]);
                else return;
            } else if (percentage > 25 && percentage <= 50) {
                if (i <= 2)
                    addItems.add(kitItems[new Random().nextInt(itemCount)]);
                else return;
            } else if (percentage > 50 && percentage <= 75) {
                if (i <= 4)
                    addItems.add(kitItems[new Random().nextInt(itemCount)]);
                else return;
            } else if (percentage > 75 && percentage <= 100) {
                if (i <= 6)
                    addItems.add(kitItems[new Random().nextInt(itemCount)]);
                else return;
            }
        }

        addItems.add(new ItemStack(Material.SANDSTONE, 8));
        addItems.add(new ItemStack(Material.COOKED_BEEF, 2));

        for (ItemStack itemStack : addItems)
            playerKiller.getPlayer().getInventory().addItem(itemStack);
    }

    public Inventory chest() {
        Inventory chest = Bukkit.createInventory(null, 45, "Coffre de §a" + name);

        for (ItemStack itemStack : getItems()) {
            int amount = new Random().nextInt(16);

            while (amount == 0) {
                amount = new Random().nextInt(16);
            }

            itemStack.setAmount(amount);

            chest.addItem(itemStack);
        }

        if (name.equalsIgnoreCase("Basic")) {

        } else if (name.equalsIgnoreCase("Dresseur")) {

        } else if (name.equalsIgnoreCase("Voleur")) {

        }

        return chest;
    }
}
