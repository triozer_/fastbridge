package fr.triozer.fastbridge.game.trader;

import fr.triozer.fastbridge.game.GameKit;
import fr.triozer.fastbridge.util.Trader;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * @author Triozer
 */
public class Healer extends Trader {
    public Healer(Location location) {
        super("Pharmacie", location);
    }

    @Override
    protected void inventoryClickEvent(InventoryClickEvent event)

    {
        event.setCancelled(true);
    }

    @Override
    protected void playerInteractAtEntity(PlayerInteractAtEntityEvent event) {
        Inventory inventory = Bukkit.createInventory(event.getPlayer(), InventoryType.HOPPER,
                "Pharmacie");

        for (GameKit gameKit : Arrays.asList(GameKit.values())) {
            ItemStack itemStack = new ItemStack(gameKit.getIcon());
            ItemMeta  itemMeta  = itemStack.getItemMeta();

            itemMeta.setDisplayName("§6" + gameKit.getName());
            itemStack.setItemMeta(itemMeta);

            inventory.addItem(itemStack);
        }

        event.getPlayer().openInventory(inventory);
        event.setCancelled(true);
    }
}
