package fr.triozer.fastbridge.game;

import fr.triozer.fastbridge.FastBridge;
import fr.triozer.fastbridge.game.trader.Healer;
import fr.triozer.fastbridge.game.trader.SpecialItems;
import fr.triozer.fastbridge.game.trader.Weapon;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;

import java.util.Map;
import java.util.Random;

/**
 * @author Triozer
 */
public class Game {
    // The prefix used at the beginning of messages
    public static final String PREFIX = "§8§l[§aFastBridge§8§l] §r";

    private int max = 16; // Max players
    private int min = 1; // Min players

    private long startedTime;

    private boolean started  = false; // If the game is started
    private boolean finished = false; // If the game is finished

    private static Scoreboard gameScoreboard;

    public Game() {
        gameScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        Location redFood  = new Location(Bukkit.getWorld("world"), 13.5, 5, 2.5);
        Location redBlock = new Location(Bukkit.getWorld("world"), 13.5, 5, 14.5);
        Location redHole  = new Location(Bukkit.getWorld("world"), 75.5, 63, -226.5);

        Location blueFood  = new Location(Bukkit.getWorld("world"), 2.5, 5, 14.5);
        Location blueBlock = new Location(Bukkit.getWorld("world"), 2.5, 5, 2.5);
        Location blueHole  = new Location(Bukkit.getWorld("world"), 75.5, 63, -224.5);

        GameTeam.teams.put("red", new GameTeam("red", "§c✪ §7- ", redHole, redFood, redBlock, redHole));
        GameTeam.teams.put("blue", new GameTeam("blue", "§b✪ §7- ", blueHole, blueFood, blueBlock, blueHole));
    }

    /**
     * Start timer before start the game
     */
    public void start() {
        new BukkitRunnable() {
            private int timeStart = 30; // Time before start the game. Here because when I cancel the time reset

            @Override
            public void run() {
                if (Bukkit.getOnlinePlayers().size() < min) {
                    this.cancel();
                }

                if (timeStart == 0) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.setLevel(0);
                        player.setGameMode(GameMode.SURVIVAL);

                        player.setHealth(20.0D);
                        player.setMaxHealth(20.0D);

                        shuffle(player);

                        player.getInventory().clear();

                        GameTeam.teams.get("red").equip();
                        GameTeam.teams.get("blue").equip();

                        player.teleport(GamePlayer.getPlayer(player.getUniqueId()).getTeam().getSpawnLocation());
                    }

                    started = true;

                    Bukkit.broadcastMessage("");
                    Bukkit.broadcastMessage(Game.PREFIX + "§7La partie vient de commencer !");
                    Bukkit.broadcastMessage("");
                    Bukkit.broadcastMessage(Game.PREFIX + "§7Tu dois récupérer des ressources sur les îles d'à côté et rejoindre la base centrale.");
                    Bukkit.broadcastMessage("");

                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.sendMessage(Game.PREFIX + "§7Tu as choisi le kit : §6"
                                + GamePlayer.getPlayer(player.getUniqueId()).getKit().getName());
                        player.sendMessage("");
                    }

                    GameTeam red  = GameTeam.teams.get("red");
                    GameTeam blue = GameTeam.teams.get("blue");

                    red.getBlock().add(0, 1, 0).getBlock().setType(Material.GLASS);
                    blue.getBlock().add(0, 1, 0).getBlock().setType(Material.GLASS);

                    Bukkit.getWorld("world").spawnEntity(red.getFood(), EntityType.COW);
                    Bukkit.getWorld("world").spawnEntity(blue.getFood(), EntityType.COW);

                    startedTime = System.currentTimeMillis();

                    spwanTraders(red.getSpawnLocation());
                    spwanTraders(blue.getSpawnLocation());

                    this.cancel();
                    return;
                }

                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.setLevel(timeStart);
                }

                if ((timeStart > 1 && timeStart <= 5) || timeStart % 10 == 0) {
                    Bukkit.broadcastMessage(Game.PREFIX + "§7La partie commence dans §b" + timeStart + " secondes§7.");
                } else if (timeStart == 1) {
                    Bukkit.broadcastMessage(Game.PREFIX + "§7La partie commence dans §b" + timeStart + " seconde§7.");
                }

                timeStart--;
            }
        }.runTaskTimer(FastBridge.getInstance(), 20L, 20L);

        // THE TIMER
        Bukkit.getScheduler().runTaskTimer(FastBridge.getInstance(), () -> {
            if (!finished) {
                long different = System.currentTimeMillis() - startedTime;

                long secondsInMilli = 1000;
                long minutesInMilli = secondsInMilli * 60;
                long hoursInMilli   = minutesInMilli * 60;

                long elapsedHours = different / hoursInMilli;
                different = different % hoursInMilli;

                long elapsedMinutes = different / minutesInMilli;
                different = different % minutesInMilli;

                long elapsedSeconds = different / secondsInMilli;

                for (GamePlayer gamePlayer : GamePlayer.getPlayers())
                    gamePlayer.getGameScoreboard().setLine(1, "§7Chrono: §6" + elapsedHours + "h" + elapsedMinutes + "m" + elapsedSeconds + "s");
            }
        }, 20L, 20L);
    }

    /**
     * Spawn the trader
     */
    private void spwanTraders(Location location) {
        new Weapon(location.add(0, 1, 0)).name("§6[MARCHAND]", "§bARMURIE", "").build();
        new SpecialItems(location.add(1, 1, 0)).name("§6[MARCHAND]", "§b§k BAZAR §k", "").build();
        new Healer(location.add(0, 1, 1)).name("§6[MARCHAND]", "§cPHARMACIE", "").build();
    }

    /**
     * Randomize the player in the teams and create a new GamePlayer
     *
     * @param player
     */
    private void shuffle(Player player) {
        GameTeam team;

        if (GameTeam.temps.containsKey(player.getUniqueId())) {
            team = GameTeam.temps.get(player.getUniqueId());
        } else {
            GameTeam red  = GameTeam.teams.get("red");
            GameTeam blue = GameTeam.teams.get("blue");

            if (red.getPlayers().size() == blue.getPlayers().size()) {
                if (new Random().nextBoolean()) {
                    team = red;
                } else {
                    team = blue;
                }
            } else {
                if (red.getPlayers().size() < blue.getPlayers().size()) {
                    team = red;
                } else {
                    team = blue;
                }
            }
        }

        new GamePlayer(player, team, GameKit.random());
    }

    /**
     * Check is the game is finished
     *
     * @return true if a team has 15 points
     */
    public void checkWin() {
        for (Map.Entry<String, GameTeam> gameTeam : GameTeam.teams.entrySet()) {
            if (gameTeam.getValue().getPoint() == 15) {
                stop(gameTeam.getValue());
                return;
            } else if (gameTeam.getValue().getPlayers().isEmpty()) {
                stop(gameTeam.getValue());
                return;
            }
        }
    }

    /**
     * Start timer before stop the game and kick all players
     */
    public void stop(GameTeam gameTeam) {
        finished = true;

        if (gameTeam != null) {
            Bukkit.broadcastMessage(Game.PREFIX + "");
            Bukkit.broadcastMessage(Game.PREFIX + "§7--------------------------------------------");
            Bukkit.broadcastMessage(Game.PREFIX + "");

            String team = "";

            if ("red".equals(gameTeam.getName())) {
                team = "§crouge";
            } else if ("blue".equals(gameTeam.getName())) {
                team = "§bbleue";
            }

            Bukkit.broadcastMessage(Game.PREFIX + "§6L'équipe " + team + " §6a gagné la partie !");
            Bukkit.broadcastMessage(Game.PREFIX + "");
            Bukkit.broadcastMessage(Game.PREFIX + "§7--------------------------------------------");
            Bukkit.broadcastMessage(Game.PREFIX + "");

            Bukkit.broadcastMessage(Game.PREFIX + "§7Vous allez être expulsé dans §b5 secondes§7.");

            new BukkitRunnable() {
                @Override
                public void run() {
                    GamePlayer.getPlayers().forEach(gamePlayer -> gamePlayer.getPlayer().kickPlayer("§cLa partie est finie."));
                }
            }.runTaskLater(FastBridge.getInstance(), 20L * 5); // Time before kick when the game is finish.
        }
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }

    public static Scoreboard getGameScoreboard() {
        return gameScoreboard;
    }

    public boolean isStarted() {
        return started;
    }

    public boolean isFinished() {
        return finished;
    }
}
