package fr.triozer.fastbridge.util;

import fr.triozer.fastbridge.FastBridge;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Triozer
 */
public abstract class Trader implements Listener{
    private String           id;
    private String           title;
    private String[]         lines;
    private List<ArmorStand> armorStands;

    private Villager            villager;
    private Villager.Profession profession;

    private Location location;

    /**
     * Create a new trader
     *
     * @param title    the name of the inventory
     * @param location where the trader have to spawn
     */
    public Trader(String title, Location location) {
        armorStands = new ArrayList<>();

        this.title = title;
        this.id = UUID.randomUUID().toString().substring(4);
        this.location = location;

        Bukkit.getPluginManager().registerEvents(this, FastBridge.getInstance());
    }

    /**
     * Name the trader
     *
     * @param lines name on multiple lines
     * @return
     */
    public Trader name(String... lines) {
        this.lines = new String[lines.length];

        for (int i = 0; i < lines.length; i++) {
            this.lines[i] = lines[i];

            ArmorStand armorStand = (ArmorStand) location.getWorld().spawnEntity(location.subtract(0, 0.25, 0), EntityType.ARMOR_STAND);

            armorStand.setVisible(false);
            armorStand.setGravity(false);

            armorStand.setCustomNameVisible(false);

            if (!lines[i].equals("")) {
                armorStand.setCustomName(lines[i]);
                armorStand.setCustomNameVisible(true);
            }

            armorStands.add(armorStand);
        }

        return this;
    }

    /**
     * Set the profession of the trader
     *
     * @param profession the profession
     * @return
     */
    public Trader profession(Villager.Profession profession) {
        this.profession = profession;

        return this;
    }

    /**
     * Spawn the trader
     *
     * @return
     */
    public Trader build() {
        villager = (Villager) location.getWorld().spawnEntity(location, EntityType.VILLAGER);

        villager.setCustomName(title);
        villager.setCustomNameVisible(false);

        villager.setAI(false);
        villager.setGravity(false);
        villager.setSilent(true);
        villager.setCollidable(false);

        if (profession == null)
            villager.setProfession(Villager.Profession.LIBRARIAN);
        else
            villager.setProfession(profession);

        return this;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (!event.getInventory().getTitle().contains(title)) return;
        if (!event.getInventory().getHolder().equals(event.getWhoClicked())) return;

        inventoryClickEvent(event);
    }

    @EventHandler
    public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event) {
        if (!(event.getRightClicked() instanceof Villager)) return;
        if (!(event.getRightClicked().getCustomName().equals(title))) return;

        playerInteractAtEntity(event);
    }

    /**
     * When a player click on the inventory (shop)
     *
     * @param event the click event
     */
    protected abstract void inventoryClickEvent(InventoryClickEvent event);

    /**
     * When a player click on the trader (open shop)
     *
     * @param event the click event
     */
    protected abstract void playerInteractAtEntity(PlayerInteractAtEntityEvent event);


    public List<ArmorStand> getArmorStands() {
        return armorStands;
    }

    public String getId() {
        return id;
    }

    public Villager getVillager() {
        return villager;
    }

    public String[] getLines() {
        return lines;
    }

    public Location getLocation() {
        return location;
    }

    public String getTitle() {
        return title;
    }
}