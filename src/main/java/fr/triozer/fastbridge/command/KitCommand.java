package fr.triozer.fastbridge.command;

import fr.triozer.fastbridge.game.GameKit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author Triozer
 */
public class KitCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        ((Player) commandSender).openInventory(GameKit.inventory());

        return false;
    }
}
