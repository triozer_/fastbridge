package fr.triozer.fastbridge.listener;

import fr.triozer.fastbridge.FastBridge;
import fr.triozer.fastbridge.game.GamePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Triozer
 */
public class PlayerQuit implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);

        Player player = event.getPlayer();

        if (FastBridge.getGame().isStarted() && !FastBridge.getGame().isFinished()) {
            GamePlayer.getPlayer(event.getPlayer().getUniqueId()).getTeam().remove(player);
            GamePlayer.getPlayer(event.getPlayer().getUniqueId()).getGameScoreboard().destroy();
            event.setQuitMessage("§c§l[-] §c" + player.getName());

            FastBridge.getGame().checkWin();
        }
    }

}
