package fr.triozer.fastbridge.listener.game;

import fr.triozer.fastbridge.game.GamePlayer;
import fr.triozer.fastbridge.game.GameTeam;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author Triozer
 */
public class FoodListener implements Listener {
    private Map<Entity, Integer> entityDrops = new HashMap<>();

    @EventHandler
    public void onKillMob(EntityDeathEvent event) {
        if (event.getEntity() instanceof Cow) {
            GamePlayer gamePlayer = GamePlayer.getPlayer(event.getEntity().getKiller().getUniqueId());

            if (!entityDrops.containsKey(event.getEntity())) {
                entityDrops.put(event.getEntity(), new Random().nextInt(8));
            }

            event.getDrops().clear();
            event.getDrops().add(new ItemStack(Material.COOKED_BEEF, entityDrops.get(event.getEntity())));

            for (ItemStack itemStack : event.getDrops()) {
                gamePlayer.getPlayer().getInventory().addItem(itemStack);
            }

            event.getDrops().clear();
            event.setDroppedExp(0);

            entityDrops.remove(event.getEntity());

            populate(gamePlayer.getTeam());
        }
    }

    private void populate(GameTeam gameTeam) {
        Entity entity = Bukkit.getWorld("world").spawnEntity(gameTeam.getFood(), EntityType.COW);
        int drops = new Random().nextInt(8);

        while (drops == 0) {
            drops = new Random().nextInt(8);
        }

        entity.setCustomName("§7Vache - §6" + drops + " steacks");
        entity.setCustomNameVisible(true);

        entityDrops.put(entity, drops);
    }
}
