package fr.triozer.fastbridge.listener.game;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Triozer
 */
public class BlockListener implements Listener {
    private Map<Block, List<Player>> blockBreakers = new HashMap<>();

    @EventHandler
    public void onDamageBlock(BlockDamageEvent event) {
        if (event.getBlock().getType() == Material.GLASS) {
            if (blockBreakers.containsKey(event.getBlock())) {
                List<Player> players = blockBreakers.get(event.getBlock());

                if (!players.contains(event.getPlayer()))
                    players.add(event.getPlayer());

                blockBreakers.replace(event.getBlock(), players);
            } else {
                List<Player> player = new ArrayList<>();
                player.add(event.getPlayer());

                blockBreakers.put(event.getBlock(), player);
            }
        }
    }

    @EventHandler
    public void onBreakBlock(BlockBreakEvent event) {
        if (event.getBlock().getType() == Material.GLASS && blockBreakers.containsKey(event.getBlock())) {
            for (Player player : blockBreakers.get(event.getBlock())) {
                if (player == event.getPlayer()) {
                    player.getInventory().addItem(new ItemStack(Material.SANDSTONE, 4));
                    continue;
                }

                player.getInventory().addItem(new ItemStack(Material.SANDSTONE, 1));
            }

            blockBreakers.remove(event.getBlock());
            event.setCancelled(true);
        }

        event.getBlock().getDrops().clear();
    }
}