package fr.triozer.fastbridge.listener.game;

import fr.triozer.fastbridge.FastBridge;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Triozer
 */
public class TreeListener implements Listener {
    private void breakLeaf(World world, int x, int y, int z, Player player) {
        Block block = world.getBlockAt(x, y, z);
        byte  data  = block.getData();

        if ((data & 4) == 4) {
            return; // player placed leaf, ignore
        }

        byte  range  = 4;
        byte  max    = 32;
        int[] blocks = new int[max * max * max];
        int   off    = range + 1;
        int   mul    = max * max;
        int   div    = max / 2;

        if (validChunk(world, x - off, y - off, z - off, x + off, y + off, z + off)) {
            int offX;
            int offY;
            int offZ;
            int type;

            for (offX = -range; offX <= range; offX++) {
                for (offY = -range; offY <= range; offY++) {
                    for (offZ = -range; offZ <= range; offZ++) {
                        Material mat = world.getBlockAt(x + offX, y + offY, z + offZ).getType();

                        blocks[(offX + div) * mul + (offY + div) * max + offZ + div] = (mat == Material.LOG || mat == Material.LOG_2) ? 0 : ((mat == Material.LEAVES || mat == Material.LEAVES_2) ? -2 : -1);
                    }
                }
            }

            for (offX = 1; offX <= 4; offX++) {
                for (offY = -range; offY <= range; offY++) {
                    for (offZ = -range; offZ <= range; offZ++) {
                        for (type = -range; type <= range; type++) {
                            if (blocks[(offY + div) * mul + (offZ + div) * max + type + div] == offX - 1) {
                                if (blocks[(offY + div - 1) * mul + (offZ + div) * max + type + div] == -2)
                                    blocks[(offY + div - 1) * mul + (offZ + div) * max + type + div] = offX;

                                if (blocks[(offY + div + 1) * mul + (offZ + div) * max + type + div] == -2)
                                    blocks[(offY + div + 1) * mul + (offZ + div) * max + type + div] = offX;

                                if (blocks[(offY + div) * mul + (offZ + div - 1) * max + type + div] == -2)
                                    blocks[(offY + div) * mul + (offZ + div - 1) * max + type + div] = offX;

                                if (blocks[(offY + div) * mul + (offZ + div + 1) * max + type + div] == -2)
                                    blocks[(offY + div) * mul + (offZ + div + 1) * max + type + div] = offX;

                                if (blocks[(offY + div) * mul + (offZ + div) * max + (type + div - 1)] == -2)
                                    blocks[(offY + div) * mul + (offZ + div) * max + (type + div - 1)] = offX;

                                if (blocks[(offY + div) * mul + (offZ + div) * max + type + div + 1] == -2)
                                    blocks[(offY + div) * mul + (offZ + div) * max + type + div + 1] = offX;
                            }
                        }
                    }
                }
            }
        }

        if (blocks[div * mul + div * max + div] < 0) {
            LeavesDecayEvent event = new LeavesDecayEvent(block);
            Bukkit.getServer().getPluginManager().callEvent(event);

            if (event.isCancelled()) {
                return;
            }

            block.setType(Material.AIR);

            if (getAmount(player) <= 15) {
                player.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
            }
        }
    }

    private int getAmount(Player player) {
        int has = 0;

        for (ItemStack item : player.getInventory().getContents()) {
            if ((item != null) && (item.getType() == Material.GOLDEN_APPLE) && (item.getAmount() > 0)) {
                has += item.getAmount();
            }
        }

        return has;
    }


    public boolean validChunk(World world, int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
        if (maxY >= 0 && minY < world.getMaxHeight()) {
            minX >>= 4;
            minZ >>= 4;
            maxX >>= 4;
            maxZ >>= 4;

            for (int x = minX; x <= maxX; x++) {
                for (int z = minZ; z <= maxZ; z++) {
                    if (!world.isChunkLoaded(x, z)) {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }

    private void checkLeaves(Block block, Player player) {
        Location    loc   = block.getLocation();
        final World world = loc.getWorld();
        final int   x     = loc.getBlockX();
        final int   y     = loc.getBlockY();
        final int   z     = loc.getBlockZ();
        final int   range = 4;
        final int   off   = range + 1;

        if (!validChunk(world, x - off, y - off, z - off, x + off, y + off, z + off)) {
            return;
        }

        Bukkit.getServer().getScheduler().runTask(FastBridge.getInstance(), () -> {
            for (int offX = -range; offX <= range; offX++) {
                for (int offY = -range; offY <= range; offY++) {
                    for (int offZ = -range; offZ <= range; offZ++) {
                        if (world.getBlockAt(x + offX, y + offY, z + offZ).getType() == Material.LEAVES || world.getBlockAt(x + offX, y + offY, z + offZ).getType() == Material.LEAVES_2) {
                            breakLeaf(world, x + offX, y + offY, z + offZ, player);
                        }
                    }
                }
            }
        });
    }

    @EventHandler
    public void onCutTree(BlockBreakEvent event) {
        if (event.getBlock().getType() == Material.LOG || event.getBlock().getType() == Material.LOG_2) {
            event.setCancelled(true);

            final List<Block> bList = new ArrayList<>();

            checkLeaves(event.getBlock(), event.getPlayer());
            bList.add(event.getBlock());

            new BukkitRunnable() {
                @Override
                public void run() {
                    for (int i = 0; i < bList.size(); i++) {
                        Block block = bList.get(i);
                        if (block.getType() == Material.LOG || block.getType() == Material.LOG_2) {
                            block.setType(Material.AIR);
                            block.getDrops().clear();

                            checkLeaves(block, event.getPlayer());
                        }
                        for (BlockFace face : BlockFace.values()) {
                            if (block.getRelative(face).getType() == Material.LOG || block.getRelative(face).getType() == Material.LOG_2) {
                                bList.add(block.getRelative(face));
                            }
                        }
                        bList.remove(block);
                    }
                    if (bList.isEmpty())
                        cancel();
                }
            }.runTaskTimer(FastBridge.getInstance(), 1L, 5L);
        }
    }
}
