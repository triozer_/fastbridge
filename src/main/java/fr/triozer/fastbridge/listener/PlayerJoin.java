package fr.triozer.fastbridge.listener;

import fr.triozer.fastbridge.FastBridge;
import fr.triozer.fastbridge.game.Game;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author Triozer
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);

        Player player = event.getPlayer();

        player.teleport(Bukkit.getWorld("world").getSpawnLocation().add(0, 2, 0));

        player.getInventory().clear();
        player.setScoreboard(Game.getGameScoreboard());

        if (FastBridge.getGame().isStarted()) {
            player.setGameMode(GameMode.SPECTATOR);

            player.sendMessage(Game.PREFIX + "§7§oTu as rejoint la partie en tant que spectateur !");
        } else {
            event.setJoinMessage("§a§l[+] §a" + player.getName());

            ItemStack team     = new ItemStack(Material.STAINED_CLAY);
            ItemMeta  teamMeta = team.getItemMeta();

            teamMeta.setDisplayName("§6Choisir une équipe.");

            team.setItemMeta(teamMeta);

            player.getInventory().setItem(0, team);

            if (Bukkit.getOnlinePlayers().size() == FastBridge.getGame().getMin())
                FastBridge.getGame().start();
        }

        ItemStack hub     = new ItemStack(Material.DARK_OAK_DOOR_ITEM);
        ItemMeta  hubMeta = hub.getItemMeta();

        hubMeta.setDisplayName("§cRetourner au hub.");

        hub.setItemMeta(hubMeta);

        player.getInventory().setItem(8, hub);
    }

}