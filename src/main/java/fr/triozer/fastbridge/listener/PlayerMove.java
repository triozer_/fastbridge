package fr.triozer.fastbridge.listener;

import fr.triozer.fastbridge.FastBridge;
import fr.triozer.fastbridge.game.GamePlayer;
import fr.triozer.fastbridge.game.GameTeam;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author Triozer
 */
public class PlayerMove implements Listener {
    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if (FastBridge.getGame().isStarted() && !FastBridge.getGame().isFinished()) {
            Player     player     = event.getPlayer();
            GamePlayer gamePlayer = GamePlayer.getPlayer(player.getUniqueId());
            GameTeam   gameTeam   = gamePlayer.getTeam();

            if (player.getLocation().distance(GameTeam.teams.get("red").getHole()) <= 1
                    && gameTeam.getName() == "blue") {
                gamePlayer.getPlayer().teleport(gameTeam.getSpawnLocation());
                gameTeam.addPoint();
                FastBridge.getGame().checkWin();
            } else if (player.getLocation().distance(GameTeam.teams.get("blue").getHole()) <= 1
                    && gameTeam.getTeam().getName() == "red") {
                gamePlayer.getPlayer().teleport(gameTeam.getSpawnLocation());
                gameTeam.addPoint();
                FastBridge.getGame().checkWin();
            }
        }
    }

}