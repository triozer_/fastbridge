package fr.triozer.fastbridge.listener;

import fr.triozer.fastbridge.FastBridge;
import fr.triozer.fastbridge.game.Game;
import fr.triozer.fastbridge.game.GameKit;
import fr.triozer.fastbridge.game.GamePlayer;
import fr.triozer.fastbridge.game.GameTeam;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author Triozer
 */
public class PlayerInteract implements Listener {
    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if (event.getItem() == null) {
            return;
        }

        if (event.getItem().getType() == Material.DARK_OAK_DOOR_ITEM) {
            event.setCancelled(true);

            player.kickPlayer("§cback to hub.");
        }

        if (!FastBridge.getGame().isStarted()) {
            event.setCancelled(true);

            if (event.getItem().getType() == Material.STAINED_CLAY) {
                Inventory inventory = Bukkit.createInventory(null, InventoryType.HOPPER, "§6Choisir une équipe");

                ItemStack red        = new ItemStack(Material.WOOL, 1, (short) 14);
                ItemStack blue       = new ItemStack(Material.WOOL, 1, (short) 11);
                ItemMeta  redMeta    = red.getItemMeta();
                ItemMeta  blueMeta   = red.getItemMeta();
                ItemStack random     = new ItemStack(Material.BARRIER);
                ItemMeta  randomMeta = random.getItemMeta();

                redMeta.setDisplayName("§cÉquipe rouge.");
                blueMeta.setDisplayName("§bÉquipe bleue.");
                randomMeta.setDisplayName("§eÉquipe aléatoire.");

                red.setItemMeta(redMeta);
                blue.setItemMeta(blueMeta);
                random.setItemMeta(randomMeta);

                inventory.setItem(0, red);
                inventory.setItem(1, blue);
                inventory.setItem(4, random);

                player.openInventory(inventory);
            }

            return;
        }
    }

    @EventHandler
    public void openChest(InventoryOpenEvent event) {
        Player player = (Player) event.getPlayer();

        if (event.getInventory().getHolder() instanceof Chest || event.getInventory().getHolder() instanceof DoubleChest) {
            event.setCancelled(true);

            player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 1f, 1f);

            player.openInventory(GamePlayer.getPlayer(player.getUniqueId()).getKit().chest());
        }
    }

    @EventHandler
    public void onInteractInventory(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if (event.getCurrentItem() == null) {
            return;
        }

        if (!FastBridge.getGame().isStarted()) {
            event.setCancelled(true);

            if (event.getClickedInventory().getName().contains("Choisir une équipe")) {
                String team = null;

                if (event.getCurrentItem().getItemMeta().getDisplayName().contains("bleue")) {
                    if (GameTeam.temps.containsKey(player.getUniqueId())) {
                        GameTeam.temps.remove(player.getUniqueId());
                    }

                    GameTeam.temps.put(player.getUniqueId(), GameTeam.teams.get("blue"));
                    team = "§bbleue";
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().contains("rouge")) {
                    if (GameTeam.temps.containsKey(player.getUniqueId())) {
                        GameTeam.temps.remove(player.getUniqueId());
                    }

                    GameTeam.temps.put(player.getUniqueId(), GameTeam.teams.get("red"));
                    team = "§crouge";
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().contains("aléatoire")) {
                    if (GameTeam.temps.containsKey(player.getUniqueId())) {
                        GameTeam.temps.remove(player.getUniqueId());
                    }

                    team = null;
                }

                if (team != null) {
                    player.sendMessage(Game.PREFIX + "§7Tu as rejoint l'équipe " + team + "§7.");
                } else {
                    player.sendMessage(Game.PREFIX + "§7Tu seras ajouté dans une équipe aléatoirement.");
                }
            }

            return;
        } else if (event.getClickedInventory().getName().contains("Kits")) {
            ItemStack itemStack = event.getCurrentItem();

            if (itemStack != null)
                for (GameKit gameKit : GameKit.values()) {
                    if (gameKit.getIcon() == itemStack.getType()) {
                        player.sendMessage(Game.PREFIX + "§7Tu as choisi le kit: §6" + gameKit.getName());

                        GamePlayer.getPlayer(player.getUniqueId()).setKit(gameKit);
                    }
                }

            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!FastBridge.getGame().isStarted()) {
            event.setCancelled(true);
            return;
        }
    }
}