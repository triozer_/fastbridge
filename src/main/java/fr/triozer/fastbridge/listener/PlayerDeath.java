package fr.triozer.fastbridge.listener;

import fr.triozer.fastbridge.FastBridge;
import fr.triozer.fastbridge.game.Game;
import fr.triozer.fastbridge.game.GameKit;
import fr.triozer.fastbridge.game.GamePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * @author Triozer
 */
public class PlayerDeath implements Listener {
    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if (!FastBridge.getGame().isStarted()) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        if (event.getEntity().getKiller() != null) {
            Player     killer       = event.getEntity().getKiller();
            GamePlayer playerKiller = GamePlayer.getPlayer(killer.getUniqueId());

            playerKiller.kill();

            event.setDeathMessage(Game.PREFIX + "§e" + player.getName() + " §7s'est fait tuer par §c" + killer.getName());

            GameKit.drop(playerKiller);
        } else {
            event.setDeathMessage(Game.PREFIX + "§e" + player.getName() + " §7est mort.");
        }

        event.getDrops().clear();
        event.setDroppedExp(0);

        GamePlayer.getPlayer(player.getUniqueId()).death();

        FastBridge.getGame().checkWin();
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        Player     player     = event.getPlayer();
        GamePlayer gamePlayer = GamePlayer.getPlayer(player.getUniqueId());

        player.sendMessage(Game.PREFIX + "§7Kit actuel : §6" + gamePlayer.getKit().getName());
        player.getInventory().addItem(GamePlayer.getPlayer(player.getUniqueId()).getKit().getItems());

        event.setRespawnLocation(GamePlayer.getPlayer(player.getUniqueId()).getTeam().getSpawnLocation());
    }
}
