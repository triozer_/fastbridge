package fr.triozer.fastbridge;

import fr.triozer.fastbridge.command.KitCommand;
import fr.triozer.fastbridge.game.Game;
import fr.triozer.fastbridge.listener.*;
import fr.triozer.fastbridge.listener.game.BlockListener;
import fr.triozer.fastbridge.listener.game.FoodListener;
import fr.triozer.fastbridge.listener.game.OreListener;
import fr.triozer.fastbridge.listener.game.TreeListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Triozer
 */
public class FastBridge extends JavaPlugin {
    private static FastBridge instance;
    private static Game       game;

    @Override
    public void onEnable() {
        instance = this;
        game = new Game();

        registerListeners();
        registerCommands();
    }

    @Override
    public void onDisable() {
        Bukkit.getWorld("world").getEntities().forEach(Entity::remove);
    }

    /*
     * Register all the listeners for the game
     */
    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new PlayerDeath(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerInteract(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerMove(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerQuit(), this);

        Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
        Bukkit.getPluginManager().registerEvents(new OreListener(), this);
        Bukkit.getPluginManager().registerEvents(new FoodListener(), this);
        Bukkit.getPluginManager().registerEvents(new TreeListener(), this);
    }

    private void registerCommands() {
        getCommand("kit").setExecutor(new KitCommand());
    }

    public static FastBridge getInstance() {
        return instance;
    }

    public static Game getGame() {
        return game;
    }
}
